
topLayer = 10;


function swim(element) {

    element.style.zIndex = topLayer;
    topLayer++;

}

function hide() {

    cover = document.getElementsByClassName("cover");
    reset();
    swim(cover[0]); 
}

function dive(cover) {
    
    cover.style.zIndex = 0; 
}

function reset() {
    
    pics = document.getElementsByClassName("resize-drag");

    for(i = 0; i < pics.length; i++){

        target = pics[i];

        target.style.width = "240px";
        target.style.height = "160px";
        //pics[i].style.margin = "50px 50px";
        //pic.data-y = 50;
        target.style.transform = target.style.webkitTransform = "translate(0px, 0px)";


        target.setAttribute("data-x", "0");
        target.setAttribute("data-y", "0");


    }

}

// target elements with the "draggable" class
interact('.draggable')
  .draggable({
    // enable inertial throwing
    inertia: true,
    // keep the element within the area of it's parent
    restrict: {
      restriction: "parent",
      endOnly: true,
      elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
    },
    // enable autoScroll
    autoScroll: true,

    // set z Index:

    // call this function on every dragmove event
    onmove: dragMoveListener,
    // call this function on every dragend event
    onend: function (event) {
/*
      var textEl = event.target.querySelector('p');

      textEl && (textEl.textContent =
        'moved a distance of '
        + (Math.sqrt(Math.pow(event.pageX - event.x0, 2) +
                     Math.pow(event.pageY - event.y0, 2) | 0))
            .toFixed(2) + 'px');
*/

    }
  });

  function dragMoveListener (event) {
    var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    // translate the element
    target.style.webkitTransform =
    target.style.transform =
      'translate(' + x + 'px, ' + y + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
  }

  // this is used later in the resizing and gesture demos
  window.dragMoveListener = dragMoveListener;








interact('.resize-drag')
  .draggable({
    onmove: window.dragMoveListener,
    restrict: {
      restriction: 'parent',
      elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
    },
  })
  .resizable({
    // resize from all edges and corners
    edges: { left: true, right: true, bottom: true, top: true },

    // keep the edges inside the parent
    restrictEdges: {
      outer: 'parent',
      endOnly: true,
    },

    // minimum size
    restrictSize: {
      min: { width: 100, height: 50 },
    },

    inertia: true,
  })
  .on('resizemove', function (event) {

    var target = event.target,
        x = (parseFloat(target.getAttribute('data-x')) || 0),
        y = (parseFloat(target.getAttribute('data-y')) || 0);

	
   	 // update the element's style
	    target.style.width  = event.rect.width + 'px';
 	   //target.style.height = event.rect.height + 'px';
		target.style.height = (event.rect.width * 0.666) + 'px';
	
	

    // translate when resizing from top or left edges
    	x += event.deltaRect.left * 0.5;
	y += (event.deltaRect.left * 0.666 * 0.5);

	x += event.deltaRect.right * -0.5;
	y += (event.deltaRect.right * 0.666 * -0.5);


    target.style.webkitTransform = target.style.transform =
        'translate(' + x + 'px,' + y + 'px)';

    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
   // target.textContent = Math.round(event.rect.width) + '\u00D7' + Math.round(event.rect.height) + ' (left: ' + event.deltaRect.left + ', top: ' + event.deltaRect.right + ', right: ' + event.deltaRect.right + ', bottom: ' + event.deltaRect.bottom + ')';
  });





